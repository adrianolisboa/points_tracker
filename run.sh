#! /bin/sh

set -e

# Ensure the app's dependencies are installed
mix deps.get

# set up the database
mix ecto.setup

echo "Launching Phoenix web server..."
mix phx.server

