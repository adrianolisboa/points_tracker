defmodule PointsTrackerWeb.UserControllerTest do
  use PointsTrackerWeb.ConnCase, async: false

  alias PointsTracker.Points.Tracker
  alias PointsTracker.Repo

  setup do
    tracker_pid = start_supervised!({Tracker, %{}})
    Ecto.Adapters.SQL.Sandbox.allow(Repo, self(), tracker_pid)
  end

  describe "GET / [index]" do
    test "list two users and timestamp", %{
      conn: conn
    } do
      insert_list(10, :user, points: 100)

      assert %{
               "timestamp" => _,
               "users" => users
             } =
               conn
               |> get(Routes.user_path(conn, :index))
               |> json_response(:ok)

      assert Enum.count(users) == 2
    end
  end
end
