defmodule PointsTracker.UsersTest do
  use PointsTracker.DataCase, async: true

  alias PointsTracker.Users

  describe "update_all_users_points/0" do
    test "it updates all points with random value" do
      _users_list = insert_list(100, :user)

      assert Users.update_all_users_points() == {:ok, :users_points_updated}
    end
  end

  describe "list_two_users_with_points_greater_than/1" do
    test "returns users with points greater than given" do
      insert(:user, points: 90)
      insert(:user, points: 80)
      insert(:user, points: 50)

      assert {:ok, users} = Users.list_two_users_with_points_greater_than(51)
      assert Enum.count(users) == 2
    end
  end
end
