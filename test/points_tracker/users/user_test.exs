defmodule PointsTracker.Users.UserTest do
  use PointsTracker.DataCase, async: true

  alias PointsTracker.Users.User

  describe "changeset/2" do
    test "valid attributes" do
      attrs = %{
        points: 100
      }

      changeset = User.changeset(attrs)

      assert changeset.valid?
      assert errors_on(changeset) == %{}
    end

    test "with negative points returns error" do
      attrs = %{
        points: -1
      }

      changeset = User.changeset(attrs)

      refute changeset.valid?
      assert errors_on(changeset) == %{points: ["must be greater than or equal to 0"]}
    end

    test "with numbers greater than 100 for points returns error" do
      attrs = %{
        points: 101
      }

      changeset = User.changeset(attrs)

      refute changeset.valid?
      assert errors_on(changeset) == %{points: ["must be less than or equal to 100"]}
    end
  end
end
