defmodule PointsTracker.Points.TrackerTest do
  use PointsTracker.DataCase, async: true

  alias PointsTracker.Points.Tracker

  setup do
    tracker_pid = start_supervised!({Tracker, %{}})
    Ecto.Adapters.SQL.Sandbox.allow(Repo, self(), tracker_pid)
  end

  describe "handle_info/1" do
    test "calls handle_info to update points and max_number " do
      insert(:user, points: 90)

      old_state = %{timestamp: nil, max_number: 100}

      assert {:noreply, _newstate} =
               Tracker.handle_info(
                 :points_update,
                 old_state
               )
    end
  end

  describe "handle_call/3" do
    test "it returns users with points greater than max_number" do
      first_user = insert(:user, points: 90)
      _second_user = insert(:user, points: 30)

      old_state = %{timestamp: nil, max_number: 40}

      {:reply, {:ok, result}, new_state} =
        Tracker.handle_call(
          :list_two_users,
          %{},
          old_state
        )

      first_user_returned = Enum.at(result.users, 0)
      assert first_user_returned.id == first_user.id
      assert first_user_returned.points == first_user.points

      assert new_state.timestamp != nil
      assert new_state.max_number == 40
    end
  end

  describe "list_two_users_with_points_greater_than_max_number" do
    test "it returns users with points greater than max_number" do
      [_first_user, _second_user] = users = insert_list(2, :user, points: 100)

      {:ok, %{timestamp: timestamp, users: result_users}} =
        Tracker.list_two_users_with_points_greater_than_max_number()

      assert timestamp == nil

      Enum.each(result_users, fn user ->
        assert a_user = Enum.find(users, &(&1.id == user.id))
        assert a_user.points == 100
      end)
    end
  end
end
