defmodule PointsTracker.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table("users") do
      add :points, :integer, default: 0

      timestamps()
    end

    create constraint("users", "points_range",
             check: "points >= 0 and points <= 100",
             comment: "Points must be from 0 to 100"
           )
  end
end
