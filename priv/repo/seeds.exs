# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     PointsTracker.Repo.insert!(%PointsTracker.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias PointsTracker.Repo
alias PointsTracker.Users.User

schemas = [
  User
]

Enum.each(schemas, &Repo.delete_all/1)

now = DateTime.utc_now()
user_changeset = %{points: 0, inserted_at: now, updated_at: now}

1..1_000_000
|> Enum.map(fn _index -> user_changeset end)
|> Enum.chunk_every(10_000)
|> Enum.each(&Repo.insert_all(User, &1))
