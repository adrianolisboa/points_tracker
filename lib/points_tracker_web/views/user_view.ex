defmodule PointsTrackerWeb.UserView do
  use PointsTrackerWeb, :view

  @fields [:id, :points]

  def render("index.json", %{data: %{users: users, timestamp: timestamp}}) do
    %{timestamp: timestamp, users: render_many(users, __MODULE__, "user.json")}
  end

  def render("user.json", %{user: user}), do: Map.take(user, @fields)
end
