defmodule PointsTrackerWeb.UserController do
  use PointsTrackerWeb, :controller

  alias PointsTracker.Points.Tracker

  def index(conn, _params) do
    {:ok, data} = Tracker.list_two_users_with_points_greater_than_max_number()

    conn
    |> put_status(:ok)
    |> render("index.json", data: data)
  end
end
