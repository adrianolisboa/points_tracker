defmodule PointsTracker.Repo do
  use Ecto.Repo,
    otp_app: :points_tracker,
    adapter: Ecto.Adapters.Postgres
end
