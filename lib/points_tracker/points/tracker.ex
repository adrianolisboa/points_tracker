defmodule PointsTracker.Points.Tracker do
  use GenServer

  alias PointsTracker.Users

  @one_minute 60_000

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_opts) do
    schedule_points_update()
    initial_state = %{timestamp: nil, max_number: random_number()}
    {:ok, initial_state}
  end

  def handle_info(:points_update, state) do
    Task.start_link(&Users.update_all_users_points/0)
    schedule_points_update()
    {:noreply, Map.put(state, :max_number, random_number())}
  end

  def handle_call(:list_two_users, _from, state) do
    {:ok, users} = Users.list_two_users_with_points_greater_than(state.max_number)
    result = {:ok, %{users: users, timestamp: state.timestamp}}
    {:reply, result, Map.put(state, :timestamp, now())}
  end

  def list_two_users_with_points_greater_than_max_number,
    do: GenServer.call(__MODULE__, :list_two_users)

  defp random_number, do: Enum.random(0..100)
  defp now, do: DateTime.utc_now()
  defp schedule_points_update, do: Process.send_after(self(), :points_update, @one_minute)
end
