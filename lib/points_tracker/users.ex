defmodule PointsTracker.Users do
  import Ecto.Query

  alias PointsTracker.Users.User
  alias PointsTracker.Repo

  def update_all_users_points do
    User
    |> update(set: [points: fragment("floor(random()*100)"), updated_at: ^DateTime.utc_now()])
    |> Repo.update_all([])

    {:ok, :users_points_updated}
  end

  def list_two_users_with_points_greater_than(max_number) do
    User
    |> where([u], u.points > ^max_number)
    |> select([u], %{id: u.id, points: u.points})
    |> limit(2)
    |> Repo.all()
    |> as_result()
  end

  defp as_result(result), do: {:ok, result}
end
