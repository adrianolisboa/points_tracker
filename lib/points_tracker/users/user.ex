defmodule PointsTracker.Users.User do
  use PointsTracker.Schema

  schema "users" do
    field :points, :integer

    timestamps()
  end

  def changeset(user \\ %__MODULE__{}, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_number(:points, greater_than_or_equal_to: 0)
    |> validate_number(:points, less_than_or_equal_to: 100)
  end
end
