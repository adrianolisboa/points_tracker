# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :points_tracker,
  ecto_repos: [PointsTracker.Repo]

config :points_tracker, PointsTracker.Repo,
  migration_primary_key: [type: :binary_id],
  migration_foreign_key: [type: :binary_id],
  migration_timestamps: [type: :utc_datetime_usec]

# Configures the endpoint
config :points_tracker, PointsTrackerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mE0RjLtn94lcrDbDbZ+xxY0vhxF/218gSDzwlAkHRxIzYEgbylwoXiQ03Xcd2Ol4",
  render_errors: [view: PointsTrackerWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: PointsTracker.PubSub,
  live_view: [signing_salt: "LgpzcdsJ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
