# User Points Tracker

This project will track user points update running a GenServer that updates all user points every minute.

## Manually

Ensure that postgres is running with user `postgres` and password `postgres` on port `5432`.

Then run:

```sh
$ mix deps.get
$ mix ecto.setup
$ mix phx.server

```

## Automatically (with Docker)

  * Start postgres with the command `docker-compose up -d`
  * Run the application `sh run.sh`

Now you can visit [`localhost:3000`](http://localhost:3000) from your browser.


## Running the tests

To execute all tests:

`mix test`

To check code coverage and generate a report:

`mix coveralls.html`

To run code analysis:

`mix credo`

To format & check if code is formatted:

` mix format && mix format --check-formatted`
